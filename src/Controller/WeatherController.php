<?php

namespace Drupal\weather\Controller;

use Drupal\Core\Controller\ControllerBase;

class WeatherController extends ControllerBase
{
  public function index()
  {
    $city = "Atlanta";
    $weatherData = $this->fetchWeather($city);
    $weather = json_decode($weatherData, true);

    $main = $weather['weather'][0]['main'];
    $description = $weather['weather'][0]['description'];

    return [
      '#theme'       => 'weather_template',
      //'#markup' => $this->t('Welcome to the Weather APP!'),
      //'#weather' => $weather,
      '#main'        => $main,
      '#description' => $description,
      '#city'        => $city,
      '#map'         => ''
    ];
  }

  /**
   * @param string $city
   * @param string $apiKey
   * @return string
   */
  private function fetchWeather($city = "Atlanta", $apiKey = "ee3d51f81347de937b9e0c1c09cd00b0")
  {
    $url = "http://api.openweathermap.org/data/2.5/weather?q={$city}&appid={$apiKey}";

    $content = '{}';

    $client = \Drupal::httpClient();
    try {
      $request = $client->get($url);
      $status = $request->getStatusCode();
      $content = $request->getBody()->getContents();
    } catch (RequestException $e) {
      //An error happened.
    }

    return $content;
  }

}
